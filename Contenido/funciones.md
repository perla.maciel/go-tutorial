# Funciones en Go
Una función o subrutina es una sección de código que realiza una tarea en específico. Todos los programas cuentan con, al menos, una función main (principal) con la posibilidad de definir más funciones para realizar subtareas. Todas las funciones se diferencian entre ellas, y de las variables, por medio de un identificador único.

La sintaxis de la declaración de funciones en Golang es la siguiente:

```
Declaración_funciones = "func" Nombre_función ( Función | Firma )
Nombre_función = identificador
Función = Firma Cuerpo_función
Firma = Parámetros [Resultado]
Parámetros = "(" [ Lista_parámetros [ "," ] ] ")"
Resultado = Parámetros | Tipo
Tipos = Nombre_tipo | Literal_tipo | "(" Tipo ")"
Cuerpo_funcion = Bloque
Bloque = "{" Lista_declaraciones "}"
```
En base a lo anterior, podemos identificar:

* func es la palabra reservada que inicia la declaración de una función.
* Firma es la lista de parámetros.
* Nombre_función es el identificador de nuestra función.
* Parámetros son los valores que se pasan a una función para trabajar dentro de ella. Puede haber cualquier cantidad de parámetros, o ninguno.
* Tipo es la lista de resultados que devuelve una función por medio de un return. Puede haber cualquier cantidad de resultados, o ninguno. El número de ellos debe coincidir con los devueltos por la sentencia return en cantidad y tipo.
* Cuerpo_función contiene las instrucciones de la función. Si se especificó un resultado, en esta sección debe de colocarse un return con los correspondientes datos a regresar.

## Ejemplo de una funcion
La siguiente función se llama mult, recibe dos parámetros enteros (numero1 y numero2), su tarea es multiplicar esos números que recibió y devolver el producto:

```go 
func mult(numero1, numero2 int) int{
  /*Variable local de la función*/
  var resultado int
  resultado = numero1 * numero2
  return resultado
}
```

## LLamado de una función
La función del ejemplo anterior teóricamente multiplicaba dos números, sin embargo, si no se le hace un llamado, solamente será código sin uso. Si se llama a una función, el control del programa se transfiere a esta última hasta que se alcanza una sentencia return, o hasta que se terminan las instrucciones dentro de la sección del cuerpo.

Para hacer un llamado a función, se utiliza el identificador de la función (nombre) y se pasan los parámetros necesarios. Si la función regresa algún valor, este puede ser almacenado en variables.

Ejemplo:
```go
package main
import "fmt"

func main(){
  /*Variable local*/
  var res int
  /*Llamado a función*/
  fmt.Printf("El resultado de 122*120: %d\n", mult(122, 120))
  /*Llamado con parámetros distintos*/
  fmt.Printf("El resultado de 130*105: %d\n", mult(130, 105))
  /*Se almacena el resultado en una variable*/
  res = mult(20, 178)
  fmt.Printf("Resultado almacenado en una variable: %d\n", res)
}
/*Función que multiplica dos números*/
func mult(numero1, numero2 int) int{
  /*Variable local de la función*/
  var resultado int
  resultado = numero1 * numero2
  return resultado
}
```
Resultado:

![](../Imagenes/funcion1.png)

## Funciones que regresan más de un valor
A diferencia de otros lenguajes de programación, las funciones en Go pueden regresar más de un valor con un return. El siguiente ejemplo muestra una función que regresa dos valores, sin embargo, cabe aclarar que puede ser devuelta cualquier cantidad de valores de cualquier tipo:

```go
package main
import "fmt"

func main(){
   /*Llamado a la función*/
  min, max := minmax(12, 23, 54)
  fmt.Printf("Min: %d\nMax: %d\n", min, max)
}
/*Función para encontrar el MÍNIMO
y MÁXIMO entre tres números*/
func minmax(a, b, c int) (int, int){
  /*Variables locales*/
  var min, max int
  /*Se encuentra el MÁXIMO, luego el MÍNIMO*/
  /*Primero verifica si "a" es el MÁXIMO*/
  if a > b && a > c{
    max = a
    if b < c{
      min = b
    }else{
      min = c
    }
  /*Si "a" no fue el MÁXIMO, prueba con "b"*/
  }else if b > a && b > c{
    max = b
    if a < c{
      min = a
    }else{
      min = c
    }
  /*Si "a" y "b" no fueron los MÁXIMOS, prueba con "c" */
  }else{
    max = c
    if a < b{
      min = a
    }else{
      min = b
    }
  }
  return min, max
}
```
Resultado:

![](../Imagenes/funcion2.png)





***
Si tiene problemas con este tutorial, por favor, [hágamelo saber](Contacto.md) y lo mejoraré. Si te gusta este tutorial, por favor dale una estrella.

Puede utilizar este tutorial libremente. [Ver LICENCIA](LICENSE).