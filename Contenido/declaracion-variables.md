# Declarando variables

Todas las variables en Go tienen un tipo por el cual rigen su valor y su representación. Los tipos de datos de las variables son estaticos, es decir que una variable no puede cambiar su tipo de dato durante su ciclo de vida. Los valores iniciales, de las variables a las cuales no se les ha asignado ningún valor, son asignados a su valor “zero” (0, "", false o null). La gran diferencia con otros lenguajes de programación como C, es que Go permite la declaración de variables con un tipo dependiente del tipo de dato de otra variable (esto es conocido como tipo dinámico o inferencia de tipo).

La estructura de la decleración de una variable es la siguiente:

```go
var variables tipos_de_dato
```

Ejemplos en codigo:

```go
package main

func main(){

    //Declaracion de una sola variable
    var x int //Variable llamada x del tipo int

    //Declaración de tres variables del mismo tipo.
    var x, y, z int 

    //

}
```
* var se usa para declarar una o más variables.
* Puedes declarar múltiples variables en una línea.
* Las variables declaradas sin su inicialización correspondiente son de valor-cero. Por ejemplo, el valor cero de una varibale de tipo int es 0.

La inicialización de variables puede ocurrir durante cualquier parte de la ejecución del programa o durante la declaración.
Ejemplos de inicialización:
```go
//Inicializar variable despues de declararla
var x int
x = 5

//Inicializar variable en la declaración
var x = 3 //Cuando se inicializa en la declaranción, no es necesario indicar el tipo de variable.

//Tambien es posible especificar el tipo de dato.
var x int = 3

//Inicializar mas de una variable durante la declaración
var x, y, z = 3, 4.0, "Hola" //Cada una de las variables toma su tipo correspondiente (x=int, y=float, z=string)

//Si necesitamos que una variable sea del mismo que otra que ya esta declarada, se hace de la siguiente manera:
var x = "Hola mundo" //Declaramos una variable llamada "x" del tipo string
y := x //La variable "y" toma el mismo tipo de dato que "x". 
```
* La syntaxis := es la abreviación para declarar e inicializar una variable con el mismo tipo al que se esta igualando, e.g. de y := x en este caso.

Las variables pueden ser declaradas dentro de una función o por fuera de cualquier función. Las que son declaradas dentro de una función se llaman variables locales, las que son declaradas por fuera de una función se llaman variables a “nivel de paquete”, las cuales pueden ser usadas en cualquier archivo .go que pertenezca al mismo paquete. Y aquellas variables declaradas a nivel de paquete que sean declaradas con una letra mayúscula al principio, son variables públicas que pueden ser accedidas desde cualquier paquete.




#### Referencias
* http://goconejemplos.com/variables
* https://medium.com/@golang_es/variables-en-go-fc77e56879ef
* https://codingornot.com/02-go-to-go-sintaxis-tipo-de-datos-y-palabras-reservadas
***
Si tiene problemas con este tutorial, por favor, [hágamelo saber](Contacto.md) y lo mejoraré. Si te gusta este tutorial, por favor dale una estrella.

Puede utilizar este tutorial libremente. [Ver LICENCIA](LICENSE).
