# Instalar Go

Si deseas aprender a programar en Go utilizando este tutorial, debes probar los ejemplos que se presentan en código. Puedes usar un sitio web como [golang.org](https://play.golang.org), pero es recomendable instalar Go. De esa manera, no necesitaraas abrir un navegador web solo para escribir código y puede trabajar sin conexión a internet.

Para este tutorial, se utilizara linux como sistema operativo, usaremos la distribucion ubuntu 18.04 LTS.

## Instalar Go en Ubuntu 18.04 LTS

La instalacion de Go en Ubuntu es bastante sencilla. Los pasos para hacerlo son los siguientes:

1. Abrir la terminal de Ubuntu.
2. Actualizamos los repositorios de Ubuntu con:
```linux
sudo apt update
```
3. Instalamos Go con el siguiente comando:
```linux
sudo apt-get install golang-go
```
![](../Imagenes/instalar1.gif)

### Verificando si Go se instalo correctamente
Para verificar si la instalación se realizo correctamente escribimos "go" en la terminal y presionamos enter. Si la instalacion fue exitosa nos apareceran la lista de comandos que podemos utilizar en Go.
![](../Imagenes/instalar2.gif)




***
Si tiene problemas con este tutorial, por favor, [hágamelo saber](Contacto.md) y lo mejoraré. Si te gusta este tutorial, por favor dale una estrella.

Puede utilizar este tutorial libremente. [Ver LICENCIA](LICENSE).