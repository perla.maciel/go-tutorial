# Estructura de control: For

La única sentencia de ciclos existente en Golang es for, contrario a muchos otros lenguajes de programación que permiten algunas como while. La buena noticia es que for abarca todos los posibles usos que podrían tener otras sentencias similares, así que, realmente no se extraña mucho el while.

## Ciclo for con condicion de paro

En el siguiente ejemplo se puede apreciar la sintaxis cuando se proporciona solamente una condición para detener el ciclo (while en otros lenguajes de programacion):

```go
package main
import "fmt"

func main(){
  var i = 0
  for i < 10 { //El ciclo se va a ejecutar mientras i sea menor que 10.
    fmt.Println("Valor de i:", i)
    i++
  }
}
```
Resultado:

![](../Imagenes/for-condicion-paro.png)

## Ciclo for con inicialización, condición e incremento
El siguiente ejemplo ilustra el funcionamiento de esta clase de ciclos:

```go
package main
import "fmt"

func main(){
  var i int
  for i=0 ; i < 10; i++ { //Dentro del ciclo se inicializa la variable i en cero, se ejecuta el for mientras i sea menor que 10 y al final del bloque de codigo aumenta en 1 la variable i.
    fmt.Println("Valor de i:", i)
  }
}
```
Resultado:

![](../Imagenes/for-con-inicializacion.png)

## Ciclo for con un rango
Para comprender el funcionamiento de esta clase de for puede utilizarse el ejemplo siguiente:

```go
package main
import "fmt"

func main(){
  arreglo:=[7]int{0,1,4,6,10,9}
  for i, j:= range arreglo{
    fmt.Printf("Valor de j: %d en vuelta #%d\n", j,i)
  }
  for i:= range arreglo{
    fmt.Printf("Valor de i: %d\n", i)
  }
}
```
Resultado:

![](../Imagenes/for-con-rango.png)

## Sentencia break

Go cuenta con una sentencia break cuyos usos son terminar el ciclo más interno en el que esté situado, o bien terminar un case de un switch.

```go
package main
import "fmt"

func main(){
  for i:=0 ; i < 10; i++ {
    fmt.Printf("Valor de i: %d", i)
    if i == 7{
      fmt.Printf(" así que saldremos del ciclo...\n")
      break
    }
    fmt.Printf("\n")
  }
}
```
Resultado:

![](../Imagenes/for-break.png)
## Sentencia continue
La sentencia continue de Go sigue un funcionamiento similar al de break con la diferencia de que continue no termina el ciclo por completo, simplemente termina la iteración actual ignorando el código restante y continúa evaluando la condición de la siguiente iteración.
El siguiente ejemplo ilustra el funcionamiento de la sentencia:

```go
package main
import "fmt"

func main(){
  var i = 0
  for i < 10 {
    fmt.Printf("Valor de i: %d", i)
    if i == 6{
      fmt.Printf(" sumaremos 3\n")
      i = i + 3
      continue
    }
    fmt.Printf("...\n")
    i++
  }
}
```
Resultado:

![](../Imagenes/for-continue.png)

## Sentencia goto
Una sentencia goto permite el “salto” hasta una etiqueta dentro de la misma función. Una etiqueta es un identificador (regularmente escrito con mayúsculas) seguido de dos puntos, el cual sirve para poner “marcas” o “puntos de referencia” en nuestros programas. Si se utiliza goto, el flujo del programa continúa de forma estructurada a partir de la etiqueta que se especificó.

No es recomendable usar sentencia que permita esta clase de movimientos en el código porque dificultan su comprensión, y siempre existe una forma de escribir cualquier programa que utiliza goto de una forma en que se omita su uso.

El siguiente ejemplo muestra el uso de goto:
```go
package main
import "fmt"
func main() {
  var i int = 0
  CICLO: for i < 10 {
    if i == 6 {
      i = i + 3
      fmt.Println("Saltando a etiqueta CICLO con i = i + 3")
      goto CICLO
    }
    fmt.Printf("Valor de i: %d\n", i)
    i++
  }
}
```
Resultado:

![](../Imagenes/for-goto.png)





***
Si tiene problemas con este tutorial, por favor, [hágamelo saber](Contacto.md) y lo mejoraré. Si te gusta este tutorial, por favor dale una estrella.

Puede utilizar este tutorial libremente. [Ver LICENCIA](LICENSE).