# Estructura de contol: if

Regularmente cuando programamos nuestros algoritmos, llega un momento donde las acciones que se deben realizar dependerán un poco o mucho del valor o el estado de alguna variable (o constante) que especifiquemos. Por ejemplo: si nuestro programa está destinado a realizar unos cálculos con valores de entrada del usuario, y dependiendo de la magnitud de ese cálculo se sume o reste algún valor N para acotar el resultado; en ese caso es necesario recurrir a sentencias condicionales que verifiquen la magnitud de nuestra variable.

## Sentencia if

La sentencia if (si, en inglés) verifica que la expresión que se le indique sea verdadera para entonces ejecutar la sección de código destinada solamente a ejecutarse cuando eso suceda. Si la condicional es falsa, se continúa ejecutando el código de forma secuencial.
Ejemplo:

```go
package main
import "fmt"

func main() {
  /*variable local de tipo entero*/
  var calificacion int = 5
  /*Sentencia if, que verifica si calificación es menor a 6*/
  if calificacion < 6 {
    /*Si la condición se cumple, imprime*/
    fmt.Println("Reprobaste")
  }
  fmt.Println("Tu calificación fue de: ", calificacion)
}
```
Resultado:

![](../Imagenes/if.png)

A diferencia de C, en Go es obligatorio incluir las sentencias después de if entre llaves independientemente de la cantidad de líneas que haya.

## Sentencia if-else

Cuando necesitamos que el programa ejecute ciertas sentencias cuando la condición es verdadera, pero a su vez necesitamos que ejecute otras sentencias solamente cuando la condición es falsa, necesitamos una sentencia condicional if-else.
Ejemplo:

```go
package main
import "fmt"

func main() {
  /*variable local de tipo entero*/
  var calificacion int = 7
  /*Sentencia if, verifica calificación menor 6*/
  if calificacion < 6 {
    /*Si la condición se cumple*/
    fmt.Println("Reprobaste")
  }else{
    /*Si la condición no se cumple*/
    fmt.Println("Aprobaste")
  }
  fmt.Println("Tu calificación fue de: ", calificacion)
}
```
Resultado:

![](../Imagenes/if-else.png)

Debe tenerse especial cuidado con la sentencia else puesto que si no se le localiza en seguida de la llave de cierre de if, podría generar un error sintáctico:

```go
if calificacion < 6{
  /*Si la condición se cumple*/
  fmt.Println("Reprobaste")
}
else{
  /*unexpected semicolon or newline before else*/
  fmt.Println("Aprobaste")
}
```
Es posible trabajar con múltiples condiciones que se verifiquen en cadena cuando la anterior no satisface por medio de else if y de ese modo verificar múltiples escenarios usando solamente sentencias if. Sin embargo debe de tenerse en cuenta que:
* Un if puede tener 0 o 1 else que funciona para señalar las sentencias que se ejecutan cuando no se cumplió ninguna de las condiciones anteriores.
* Un if puede tener de 0 a N else if siempre y cuando no estén después de un else (ya que el else, en caso de que exista, debe ser la sentencia final).

Si un else if se cumple, ninguno de los siguientes será revisado, continúa la ejecución del resto código.

El siguiente ejemplo ilustra el funcionamiento:

```go
package main
import "fmt"

func main() {
  /*variable local de tipo entero*/
  var calificacion int = 9
  /*Sentencia if, verifica calificación menor a 6*/
  if calificacion < 6 { /*Si la condición se cumple*/ fmt.Println("Reprobaste.") }else if calificacion >= 6 && calificacion <= 8 {
    /*Segunda condición*/
    fmt.Println("Aprobaste.")
  }else if calificacion == 9{
    /*Tercera condición*/
    fmt.Println("Aprobaste. Te fue muy bien.")
  }else{
    /*Si ninguna de las anteriores se cumplió*/
    fmt.Println("Felicidades. Aprobaste con calificación perfecta")
  }
  fmt.Println("Tu calificación fue de: ", calificacion)
}
```

Resultado:
![](../Imagenes/if-else-fin.png)




***
Si tiene problemas con este tutorial, por favor, [hágamelo saber](Contacto.md) y lo mejoraré. Si te gusta este tutorial, por favor dale una estrella.

Puede utilizar este tutorial libremente. [Ver LICENCIA](LICENSE).
