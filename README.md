# Tutorial de programación con Go 

Este es un tutorial practico de programación en Go para personas que empiezan con este lenguaje. Se intenta mostrar todo con ejemplos de código simple; No hay explicaciones largas y complicadas.

Este tutorial está dirigido a personas sin experiencia en programación. o muy poca experiencia en programación.

## Instalar Go

Si deseas aprender a programar en Go utilizando este tutorial, debes probar los ejemplos que se presentan en código. Puedes usar un sitio web como [golang.org](https://play.golang.org), pero es recomendable instalar Go. De esa manera, no necesitaraas abrir un navegador web solo para escribir código y puede trabajar sin conexión a internet.

Para este tutorial, se utilizara linux como sistema operativo, usaremos la distribucion ubuntu 18.04 LTS.

### Instalar Go en Ubuntu 18.04 LTS

La instalacion de Go en Ubuntu es bastante sencilla. Los pasos para hacerlo son los siguientes:

1. Abrir la terminal de Ubuntu.
2. Actualizamos los repositorios de Ubuntu con:
```linux
sudo apt update
```
3. Instalamos Go con el siguiente comando:
```linux
sudo apt-get install golang-go
```
![](../Imagenes/instalar1.gif)

#### Verificando si Go se instalo correctamente
Para verificar si la instalación se realizo correctamente escribimos "go" en la terminal y presionamos enter. Si la instalacion fue exitosa nos apareceran la lista de comandos que podemos utilizar en Go.
![](../Imagenes/instalar2.gif)

## Contenido

1. [Introducción a Go](Contenido/why-go.md)
2. [Hello, World](Contenido/hello-world.md)
3. [Tipos de datos](Contenido/tipos-datos.md)
4. [Declarando variables](Contenido/declaracion-variables.md)
5. [Estructura de control: for](Contenido/estructuras-de-control-for.md)
6. [Estructuras de control: if](Contenido/estructuras-de-control-if.md)
7. [Estructuras de control: switch](Contenido/estructuras-de-control-switch.md)
8. [Arreglos](Contenido/arreglos.md)
9. [Slices](Contenido/slices.md)
10. [Maps](Contenido/maps.md)
11. [Funciones](Contenido/funciones.md)
12. [Instalar Go](Contenido/instalar-go.md)


***
Si tiene problemas con este tutorial, por favor, [hágamelo saber](Contacto.md) y lo mejoraré. Si te gusta este tutorial, por favor dale una estrella.

Puede utilizar este tutorial libremente. [Ver LICENCIA](LICENSE).