package main
import "fmt"
func main() {
  var i int = 0
  CICLO: for i < 10 {
    if i == 6 {
      i = i + 3
      fmt.Println("Saltando a etiqueta CICLO con i = i + 3")
      goto CICLO
    }
    fmt.Printf("Valor de i: %d\n", i)
    i++
  }
}

