package main
import "fmt"

func main(){
  var i int
  for i=0 ; i < 10; i++ { //Dentro del ciclo se inicializa la variable i en cero, se ejecuta el for mientras i sea menor que 10 y al final del bloque de codigo aumenta en 1 la variable i.
    fmt.Println("Valor de i:", i)
  }
}

