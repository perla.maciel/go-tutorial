package main
import "fmt"

func main(){
  /*Variable local*/
  var res int
  /*Llamado a función*/
  fmt.Printf("El resultado de 122*120: %d\n", mult(122, 120))
  /*Llamado con parámetros distintos*/
  fmt.Printf("El resultado de 130*105: %d\n", mult(130, 105))
  /*Se almacena el resultado en una variable*/
  res = mult(20, 178)
  fmt.Printf("Resultado almacenado en una variable: %d\n", res)
}
/*Función que multiplica dos números*/
func mult(numero1, numero2 int) int{
  /*Variable local de la función*/
  var resultado int
  resultado = numero1 * numero2
  return resultado
}

