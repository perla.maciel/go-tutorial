package main
import "fmt"

func main(){
  nums1:=make([]int, 5, 5) //Nuevo slice
  nums2:=nums1[:] //Nuevo slice referenciando nums1
  fmt.Println("nums1 = ", nums1)
  nums2[1] = 99 //Modifica nums2
  //Los cambios hecho en nums2 se reflejan en nums1
  fmt.Println("nums1 después de cambios en nums2 = ",nums1)
  nums3:=make([]int, 5, 5) //nuevo slice
  //Ejemplo de copy()
  copy(nums3, nums1)
  fmt.Println("nums3 (usando copy() con nums1) = ", nums3)
  nums1[2] = 55
  //Usando copy(), nums3 no referencia a nums1
  fmt.Println("Valores de nums1 después de cambio = ", nums1)
  fmt.Println("nums3 no cambió al modificar nums1 = ", nums3)
}

