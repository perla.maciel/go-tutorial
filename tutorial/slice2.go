package main
import "fmt"

func main(){
  var palabras = []string{"ab", "cd", "fg", "hi"}
  //Inicializar un slice con un segmento del array
  slice_palab:=palabras[1:3]
  fmt.Println("Arreglo palabras= ", palabras)
  fmt.Println("Slice palabras[1:3]= ", slice_palab)
  //Funciones len() y cap() en acción
  fmt.Printf("slice_palab len = %d cap = %d\n", len(slice_palab), cap(slice_palab))
  slice_palab = append(slice_palab, "jk", "lm") //Se añaden 2 elementos
  /*Capacidad de slice_palab = 6 porque aumenta la capacidad
  * del array que referencia (palabras)*/
  fmt.Printf("slice_palab len = %d cap = %d\n", len(slice_palab), cap(slice_palab))
}

